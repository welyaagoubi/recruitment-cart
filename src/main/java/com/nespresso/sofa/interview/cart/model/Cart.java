package com.nespresso.sofa.interview.cart.model;

import static java.util.UUID.randomUUID;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.nespresso.sofa.interview.cart.printer.CartPrinter;
import com.nespresso.sofa.interview.cart.printer.CartPrinterFactory;

import java.util.UUID;

public final class Cart implements Serializable {
	
	private static final int EMPTY_QUANTITY = 0;

	private static final String SIMPLE_CART_PRINTER = "SIMPLE";

    private final UUID id;
    
    private Map<String, Integer> products;

    public Cart() {
        this(randomUUID());
    }

    public Cart(UUID id) {
        this.id = id;
        this.products = new HashMap<>();
    }

    public Cart(Map<String, Integer> products) {
        this.id = randomUUID();
        this.products = products;
    }

    public UUID getId() {
        return id;
    }

    public Map<String, Integer> getProducts() {
        return this.products;
    }
    
	public boolean addProduct(String productCode, int quantity) {
		Map<String, Integer> products = new HashMap<>(this.products);
		if (!this.containsProduct(productCode) && quantity > EMPTY_QUANTITY) {
			products.put(productCode, quantity);
			this.products = products;
			return true;
		} else if (this.containsProduct(productCode) && quantity != EMPTY_QUANTITY) {
			int productQuantity = this.renderProductQuantity(productCode);
			int combinedQuantity = productQuantity + quantity;
			if (combinedQuantity > EMPTY_QUANTITY) {
				products.put(productCode, combinedQuantity);
			} else {
				products.remove(productCode);
			}
			this.products = products;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean updateProduct(String productCode, int quantity) {
		Map<String, Integer> products = new HashMap<>(this.products);
		if (this.containsProduct(productCode)) {
			int currentProductQuantity = this.renderProductQuantity(productCode);
			if (currentProductQuantity != quantity && quantity > EMPTY_QUANTITY) {
				products.put(productCode, quantity);
				this.products = products;
				return true;
			} else if (currentProductQuantity != quantity && quantity <= EMPTY_QUANTITY) {
				products.remove(productCode);
				this.products = products;
				return true;
			} else {
				return false;
			}
		} else {
			return this.addProduct(productCode, quantity);
		}
	}
	
	public void removeProduct(String productCode) {
		if (this.containsProduct(productCode)) {
			Map<String, Integer> products = new HashMap<>(this.products);
			products.remove(productCode);
			this.products = products;
		}
	}
    
    public boolean containsProduct(String productCode) {
		return this.products.entrySet().stream().anyMatch(entry -> entry.getKey().equals(productCode));
	}

    private int renderProductQuantity(String productCode) {
    	return this.products.get(productCode);
    }
    
    public int renderProductsTotal() {
    	int total = 0;
    	for (Entry<String, Integer> product : this.products.entrySet()) {
    		total += product.getValue();
    	}
    	return total;
    }
    
	public String toString() {
    	CartPrinter cartPrinter = CartPrinterFactory.createCartPrinter(SIMPLE_CART_PRINTER);
    	return cartPrinter.printCart(this.getId(), this.products);
    }
	
}
