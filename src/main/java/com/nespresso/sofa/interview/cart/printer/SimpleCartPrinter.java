package com.nespresso.sofa.interview.cart.printer;

import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class SimpleCartPrinter implements CartPrinter {

	private static final String CART_FORMAT = "Cart {id: %s, products: {%s}}";
	private static final String PRODUCT_SEPARATOR = ",";
	
	@Override
	public String printCart(UUID cartId, Map<String, Integer> products) {
    	String idString = String.valueOf(cartId);
    	String productsString = constructProductsString(products);
    	
    	return String.format(CART_FORMAT, idString, productsString);
	}
	
	private String constructProductsString(Map<String, Integer> products) {
		StringBuilder productsBuilder = new StringBuilder();
		
    	for (Entry<String, Integer> product : products.entrySet()) {
    		productsBuilder.append(product.getKey() + "=" + product.getValue() + PRODUCT_SEPARATOR);
    	}
    	
    	String productsString = productsBuilder.toString();
    	productsString = productsString.substring(0, productsString.length() - 1);
    	
		return productsString;
	}
}
