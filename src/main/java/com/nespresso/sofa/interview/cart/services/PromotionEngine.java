package com.nespresso.sofa.interview.cart.services;

import java.util.Arrays;
import java.util.List;

import com.nespresso.sofa.interview.cart.model.Cart;

/**
 * If one or more product with code "1000" is purchase, ONE product with code 9000 is offer
 * For each 10 products purchased a gift with code 7000 is offer.
 */
public class PromotionEngine {
		
    private static final int GIFT_CONDITION = 10;
	private static final int PROMOTION_OFFER = 1;

    public static final String PRODUCT_WITH_PROMOTION = "1000";
    public static final String PROMOTION = "9000";
    public static final String GIFT = "7000";
    
    public static final List<String> BANNED_ITEMS = Arrays.asList(PROMOTION, GIFT);
	
    public Cart apply(Cart cart) {
    	cleanCart(cart);
    	awardGift(cart);
    	awardProductPromotion(cart);
        return cart;
    }

	private void awardGift(Cart cart) {
		int productsTotal = cart.renderProductsTotal();
    	cart.addProduct(GIFT, productsTotal / GIFT_CONDITION);
	}

	private void awardProductPromotion(Cart cart) {
		if (cart.containsProduct(PRODUCT_WITH_PROMOTION)) {
    		cart.updateProduct(PROMOTION, PROMOTION_OFFER);
    	}
	}

	private void cleanCart(Cart cart) {
		for (String item : BANNED_ITEMS) {
			cart.removeProduct(item);
		}
	}
}
