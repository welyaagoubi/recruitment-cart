package com.nespresso.sofa.interview.cart.printer;

public class CartPrinterFactory {

	private static final String SIMPLE_CART_PRINTER = "SIMPLE";

	public static CartPrinter createCartPrinter(String cartPrinterType) {
		if (SIMPLE_CART_PRINTER.equals(cartPrinterType)) {
			return new SimpleCartPrinter();
		} else {
			throw new IllegalArgumentException();
		}
	}
}
