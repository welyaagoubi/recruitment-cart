package com.nespresso.sofa.interview.cart.printer;

import java.util.Map;
import java.util.UUID;

public interface CartPrinter {

	public String printCart(UUID cartId, Map<String, Integer> products);
}
